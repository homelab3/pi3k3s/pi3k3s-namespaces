# Monitoring

This namespace contains all monitoring services.

## Portainer

To deploy Portainer:
```bash
kubectl apply -f portainer.yaml 
```